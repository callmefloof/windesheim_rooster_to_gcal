﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getAllWindesheim
{



    public class Calendarinfo
    {

        public string CalendarId { get; set; }
        public string Summary { get; set; }

        public Calendarinfo(string calendarId, string summary)
        {
            CalendarId = calendarId;
            Summary = summary;
        }
    }
}
